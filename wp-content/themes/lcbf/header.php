<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?></title>
    <!-- <link href='https://fonts.googleapis.com/css?family=Montserrat:wght@700&display=swap' rel='stylesheet'> -->
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c75d1f1ac2.js" crossorigin="anonymous"></script>
    <?php wp_head(); ?>
    <!-- <style>
        @import url('https://fonts.googleapis.com/css2?family=Montserrat');
    </style> -->
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <?php if (is_page('accueil')) : ?>
        <?php get_template_part('template-parts/nav'); ?>
    <?php elseif (is_page('election') || is_page('actualites') || is_page('infos-pratiques') || is_page('partenaires-exposants') || is_404() || is_page('galeries')) : ?>
        <?php get_template_part('template-parts/nav-simple', 'page'); ?>
    <?php endif; ?>