<?php /* Template Name: Home */ ?>

<?php get_header(); ?>
<div id="home">
    <main class="p-0">
        <div id="home-first-part">
            <div class=" mx-auto">
                <div class="row my-5 py-5">
                    <div class="col-md-6">
                        <div class="election-card p-0">
                            <div class="card-img-wrap">
                                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/images/cargo-bike-annee.png" alt="Card image cap">
                            </div>
                            <div class="pictos">
                                <img class="election" src="<?php echo get_template_directory_uri(); ?>/images/Picto-election-cargo-2023.png" alt="Card image cap">
                            </div>
                            <div class="centered w-100">
                                <h3>Election du vélo cargo de l'année</h3>
                            </div>
                            <div class="button">
                                <a href="<?php echo bloginfo('url'); ?>/election/">Je vote !</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="pe-card p-0">
                            <div class="card-img-wrap">
                                <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/images/partenaires-exposants.png" alt="Card image cap">
                            </div>
                            <div class="pictos">
                                <img class="pe" src="<?php echo get_template_directory_uri(); ?>/images/Picto p&e.png" alt="Card image cap">
                            </div>
                            <div class="centered w-100">
                                <h3>Partenaires & Exposants</h3>
                            </div>
                            <div class="button">
                                <a href="#">Je découvre</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div id="presentation" class="my-5 py-5">
            <div class="mx-auto">
                <div class="card">
                    <div class="row p-0 align-items-center">
                        <div class="col-xl-6 p-0 festival-img">
                            <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/images/festival-photo-01.png" alt="Card image cap">
                        </div>
                        <div class="col-xl-6 p-0 festival-presentation">
                            <h3 class="col-md-9 mx-auto mb-5 d-flex align-items-center"><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-jaune-01.png" alt="Card image cap" alt=""> <span class="mx-3">Le festival</span><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-jaune-01.png" alt="Card image cap" alt=""></h3>
                            <?php the_content(); ?>
                            <div class="col-md-9 mt-4 mx-auto btn-more"><a href="<?php bloginfo('url') ?>/le-festival">En savoir +</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="participer" class="py-5 my-5">
            <h3 class="mb-5 d-flex justify-content-center align-items-center"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""><span class="mx-3">Participer</span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""></h3>
            <div class="row">
                <div class="col-md-6 col-xl-4">
                    <div class="card participer-content p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Groupe 799.png" alt="Card image cap" alt="">
                        <div class="centered w-100">
                            <?php
                            $comment_devenir_benevole = get_post(300);
                            $cdb_title = $comment_devenir_benevole->post_title;
                            $cdb_excerpt = $comment_devenir_benevole->post_excerpt;
                            ?>
                            <div class="hide">
                                <h4 class="text-center" style="font-weight: 700;"><?php echo $cdb_title; ?></h4>
                                <div class="text-center mt-4"><i class="fa-solid fa-circle-arrow-down fa-2x"></i></div>
                            </div>
                            <div class="cd-excerpt mx-5"><?php echo $cdb_excerpt; ?>
                                <div class="btn-cd mt-5"><a href="">Je deviens bénévole</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="card p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/participer-02.png" alt="Card image cap" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="card participer-content p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Groupe 799.png" alt="Card image cap" alt="">
                        <div class="centered w-100">
                            <?php
                            $comment_devenir_partenaire = get_post(302);
                            $cdp_title = $comment_devenir_partenaire->post_title;
                            $cdp_excerpt = $comment_devenir_partenaire->post_excerpt;
                            ?>
                            <div class="hide">
                                <h4 class="text-center" style="font-weight: 700;"><?php echo $cdp_title; ?></h4>
                                <div class="text-center mt-4"><i class="fa-solid fa-circle-arrow-down fa-2x"></i></div>
                            </div>
                            <div class="cd-excerpt mx-5"><?php echo $cdp_excerpt; ?>
                                <div class="btn-cd mt-5"><a href="">Je deviens partenaire</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="card p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/participer-01.png" alt="Card image cap" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="card participer-content p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Groupe 799.png" alt="Card image cap" alt="">
                        <div class="centered w-100">
                            <?php
                            $comment_devenir_exposant = get_post(304);
                            $cde_title = $comment_devenir_exposant->post_title;
                            $cde_excerpt = $comment_devenir_exposant->post_excerpt;
                            ?>
                            <div class="hide">
                                <h4 class="text-center" style="font-weight: 700;"><?php echo $cde_title; ?></h4>
                                <div class="text-center mt-4"><i class="fa-solid fa-circle-arrow-down fa-2x"></i></div>
                            </div>
                            <div class="cd-excerpt mx-5"><?php echo $cde_excerpt; ?>
                                <div class="btn-cd mt-5"><a href="">Je deviens exposant</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4">
                    <div class="card p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/participer-03.png" alt="Card image cap" alt="">
                    </div>
                </div>
            </div>

            <?php wp_reset_postdata(); ?>
        </div>

        <div id="last-posts" class="pb-5">
            <h3 class="mb-5 d-flex justify-content-center align-items-center"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-rose-02.png" alt="Card image cap" alt=""><span class="mx-3">Nos dernières actualités</span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-rose-02.png" alt="Card image cap" alt=""></h3>
            <div class="text-center">
                <?php echo do_shortcode('[instagram-feed feed=5]'); ?>
            </div>
        </div>

    </main>
</div>
<?php get_footer(); ?>