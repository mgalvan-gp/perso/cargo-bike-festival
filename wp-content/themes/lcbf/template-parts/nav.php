<nav id="hero-home">
    <div class="hero-img">
        <div id="hero" class="row">
            <div class="navbar p-0">
                <div class="container-fluid mega-menu-nav">
                    <div class="my-3">
                        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Logo-CBF-Menu.png" alt="Image d'en-tête" class="img-fluid cbf-logo"></a>
                    </div>
                    <?php wp_nav_menu(array('theme_location' => 'max_mega_menu_1')); ?>
                </div>
            </div>
        </div>
        <img src="<?php echo get_template_directory_uri(); ?>/images/background-CBF.png" alt="cargo">
        <div class="centered col-12">
            <h1>CARGO BIKE FESTIVAL</h1>
            <h2>14 mai 2023 - LYON - Place Bellecour</h2>
        </div>
    </div>
</nav>