<nav id="header-simple-page">
    <div id="hero" class="row">
        <div class="navbar p-0">
            <div class="container-fluid mega-menu-nav">
                <div class="my-3">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Logo-CBF-2.png" alt="Image d'en-tête" class="img-fluid logo-cbf-2"></a>
                </div>
                <?php wp_nav_menu(array('theme_location' => 'max_mega_menu_1')); ?>
            </div>
        </div>
    </div>
    <div class="hero-img hero-simple-page <?php if (is_404()) : ?>no-banner<?php endif; ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/images/background-CBF.png" alt="cargo">
        <div class="centered col-12">
            <h1 class="uppercase"><?php the_title() ?></h1>
            <h2>CARGO BIKE FESTIVAL</h2>
            <img class="picto-banner" src="<?php echo get_template_directory_uri(); ?>/images/Picto-election-cargo-2023.png">
        </div>
    </div>
</nav>