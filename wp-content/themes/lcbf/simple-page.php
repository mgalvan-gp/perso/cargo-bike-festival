<?php /* Template Name: Simple Page */ ?>

<?php get_header(); ?>

<div id="simple-page" class="row text-center">
    <?php if (is_page('actualites')) : ?>
        <div class="col-md-9 mx-auto">
            <?php echo do_shortcode('[instagram-feed feed=2]'); ?>
        </div>
    <?php elseif (is_page('election')) : ?>
        <div id="election" class="my-5 py-5 px-0">
            <div class="mx-auto px-0">
                <div class="card">
                    <div class="row p-0 align-items-center">
                        <div class="col-xl-6 p-0 festival-img">
                            <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/images/election-cargo.png" alt="Card image cap">
                        </div>
                        <div class="col-xl-6 p-0 festival-presentation">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <p>Les votes sont clos</p>
        </div>
        <!-- <?php if (!is_user_logged_in()) : ?>
            <?php echo do_shortcode('[wpcdt-countdown id="110"]'); ?>
            <div class="col-md-6 mx-auto">
                <h5>Inscrivez-vous pour voter ou <a href="<?php echo home_url(); ?>/connexion/">connectez-vous</a>.</h5>
                <?php echo do_shortcode('[wpmem_form register]'); ?>
            </div>
        <?php else : ?>
            <div>
                <?php if (!current_user_can('manage_options') && is_user_logged_in()) : ?>
                    <p>Ouverture des votes dans :</p>
                    <?php echo do_shortcode('[wpcdt-countdown id="110"]'); ?>
                <?php else : ?>
                    <h3 class="d-flex align-items-center justify-content-center"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""><span class="mx-3">Les vélos cargos</span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""></h3>
                    <?php echo do_shortcode('[qsm quiz=1]'); ?>
                    <?php
                        $cargos_args = array(
                            'post_type' => 'cargos'
                        );

                        $cargos = new WP_Query($cargos_args);

                        for ($i = 1; $i <= 36; $i++) {
                    ?>
                        <?php while ($cargos->have_posts()) : $cargos->the_post(); ?>
                            <?php if ($i == get_post_meta($post->ID, 'id', true)) : ?>
                                <div id="cargo-<?php echo $i ?>" class="modal fade" aria-hidden="true" aria-labelledby="modalToggleLabel0">
                                    <div class="modal-dialog modal-dialog-centered modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body row">
                                                <div class="modal-img"><?php the_post_thumbnail(); ?></div>
                                                <div class="modal-desc">
                                                    <h5 class="card-title"><?php the_title(); ?></h5>
                                                    <h6 class="card-subtitle mb-2"><?php echo get_post_meta($post->ID, 'model', true); ?></h6>
                                                    <div class="card-body text-start">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php } ?>

                <?php endif; ?> -->
</div>
<?php endif; ?>
<?php elseif (is_page('infos-pratiques')) : ?>
    <?php $infos_args = array(
            'post_type' => 'infos',
        );

        $infos = new WP_Query($infos_args);
    ?>
    <div id="infos-pratiques" class="my-5 py-5 px-0">
        <div class="mx-auto px-0">
            <div class="card">
                <div class="row p-0 align-items-center">
                    <div class="col-xl-6 p-0 festival-img">
                        <img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/images/infos-pratiques.jpg" alt="Card image cap">
                    </div>
                    <div class="col-xl-6 p-0 festival-presentation">
                        <div class="accordion mx-auto" id="infosPratiquesCollapse">
                            <div class="infos-intro"><?php the_content(); ?></div>
                            <div class="mt-4">
                                <?php if ($infos->have_posts()) : while ($infos->have_posts()) : $infos->the_post(); ?>
                                        <a class="btn btn-link" data-bs-toggle="collapse" type="button" href="#<?php echo get_post_meta($post->ID, 'id', true); ?>">
                                            <div id="headingOne" class="collapse-header">
                                                <?php the_title(); ?>
                                            </div>
                                        </a>

                                        <div id="<?php echo get_post_meta($post->ID, 'id', true); ?>" class="collapse" data-bs-parent="#infosPratiquesCollapse">
                                            <div class="card-body">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                <?php endwhile;
                                endif;
                                wp_reset_postdata();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php elseif (is_page('partenaires-exposants')) :  ?>
    <div class="my-5 py-5">
        <?php the_content(); ?>
    </div>

    <div id="partenaires-exposants" class="mb-5">
        <div class="card pt-4 mb-5">
            <h3 class="d-flex align-items-center mx-auto my-5"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""><span class="mx-3">Partenaires</span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""></h3>
            <?php $partenaires_args = array(
                'post_type' => 'partenaires',
                'order' => 'DESC'
            );

            $partenaires = new WP_Query($partenaires_args); ?>
            <div class="row">
                <?php if ($partenaires->have_posts()) : while ($partenaires->have_posts()) : $partenaires->the_post(); ?>
                        <div class="pes-img col-md-4 d-flex align-items-center"><?php the_post_thumbnail(); ?></div>
                <?php endwhile;
                endif; ?>
            </div>
        </div>

        <div class="card pt-4 mb-5">
            <h3 class="d-flex align-items-center mx-auto my-5"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""><span class="mx-3">Sponsors</span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""></h3>
            <?php $sponsors_args = array(
                'post_type' => 'sponsors',
                'order' => 'DESC'
            );

            $sponsors = new WP_Query($sponsors_args); ?>
            <div class="row">
                <?php if ($sponsors->have_posts()) : while ($sponsors->have_posts()) : $sponsors->the_post(); ?>
                        <div class="pes-img col-md-4 d-flex align-items-center"><?php the_post_thumbnail(); ?></div>
                <?php endwhile;
                endif; ?>
            </div>
        </div>

        <div class="card pt-4 pb-5 mb-5">
            <h3 class="d-flex align-items-center mx-auto my-5"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""><span class="mx-3">Exposants</span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""></h3>
            <?php $exposants_args = array(
                'post_type' => 'exposants',
                'order' => 'DESC'
            );

            $exposants = new WP_Query($exposants_args); ?>
            <div class="row">
                <?php if ($exposants->have_posts()) : while ($exposants->have_posts()) : $exposants->the_post(); ?>
                        <div class="pes-img col-md-4 d-flex align-items-center"><?php the_post_thumbnail(); ?></div>
                <?php endwhile;
                endif; ?>
            </div>
        </div>
    </div>

    <!-- GALERIES -->
<?php elseif (is_page('galeries')) : ?>
    <?php $galleries_args = array(
            'post_type' => 'galeries',
            'orderby' => 'title'
        );

        $galleries = new WP_Query($galleries_args);
    ?>
    <div id="galeries" class="my-5 py-5">
        <div class="my-5">
            <?php if ($galleries->have_posts()) : ?>
                <? while ($galleries->have_posts()) : $galleries->the_post(); ?>
                    <button class="button-gallery btn col-md-6">
                        <?php the_title(); ?>
                    </button>

                    <div class="content-gallery col-md-9 mx-auto">
                        <?php the_content(); ?>
                    </div>
                <?php endwhile; ?>
            <?php
            endif; ?>
        </div>
    </div>

    <script>
        let buttonGallery = document.querySelectorAll('.button-gallery');
        let contentGallery = document.querySelectorAll('.content-gallery');

        for (let i = 0; i < buttonGallery.length; i++) {
            {
                buttonGallery[i].addEventListener('click', function(event) {
                    for (j of contentGallery) {
                        if (buttonGallery[i].nextElementSibling == j) {
                            if (j.style.display !== 'block') {
                                j.style.display = 'block';
                                j.style.opacity = 1;
                            } else {
                                j.style.display = 'none';
                            }
                        }
                    }
                })
            }
        }
    </script>
<?php endif; ?>
</div>

<?php get_footer(); ?>