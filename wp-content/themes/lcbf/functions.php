<?php

/**
 * Thumbnails supporting
 */
add_theme_support('post-thumbnails');

/**
 * Load assets
 */
function lcbf_register_assets()
{
    // Déclarer le fichier CSS à un autre emplacement
    wp_enqueue_style(
        'lcbf',
        get_template_directory_uri() . '/style.css',
        array(),
        '1.0'
    );
}
add_action('wp_enqueue_scripts', 'lcbf_register_assets');

/**
 * Menus supporting
 */
add_theme_support('menus');

/**
 * Login redirection
 */
// add_filter('wpmem_login_redirect', 'my_login_redirect', 10, 2);
// function my_login_redirect($redirect_to, $user_id)
// {
//     return home_url('/accueil/');
// }

/**
 * Logout (without confirmation)
 */
function getLogoutUrl($redirectUrl = '')
{
    if (!$redirectUrl) $redirectUrl = site_url();
    $return = str_replace("&amp;", '&', wp_logout_url($redirectUrl));
    return $return;
}

function logout_without_confirmation($action, $result)
{
    if (!$result && ($action == 'log-out')) {
        wp_safe_redirect(getLogoutUrl());
        exit();
    }
}
add_action('check_admin_referer', 'logout_without_confirmation', 1, 2);

// Disable the toolbar completely for all users
add_filter('show_admin_bar', '__return_false');

add_filter('wp_nav_menu_items', 'add_loginout_link', 10, 2);
function add_loginout_link($items, $args)
{
    if (is_user_logged_in() && $args->theme_location == 'Principal') {
        $items .= '<li><a href="' . wp_logout_url() . '">Log Out</a></li>';
    } elseif (!is_user_logged_in() && $args->theme_location == 'Principal') {
        $items .= '<li><a href="' . site_url('wp-login.php') . '">Log In</a></li>';
    }
    return $items;
}



add_action('login_form_register', 'ui_set_registration_username');

function ui_set_registration_username()
{
    //if there is anything set for user email
    if (isset($_POST['user_email']) && !empty($_POST['user_email'])) {
        //replace login with user email
        $_POST['user_login'] = $_POST['user_email'];
    }
}

//Remove error for username, only show error for email only.
add_filter('registration_errors', 'ui_registration_errors', 10, 3);

function ui_registration_errors($wp_error, $sanitized_user_login, $user_email)
{
    if (isset($wp_error->errors['empty_username'])) {
        unset($wp_error->errors['empty_username']);
    }

    return $wp_error;
}

//replace WP strings with our own custom strings
add_filter('gettext', 'ui_custom_string', 20, 3);
function ui_custom_string($translated_text, $text, $domain)
{
    if ($translated_text == 'Username or Email Address') {
        //you can add any string you want here, as a case
        return 'E-mail Address';
    }

    return $translated_text;
}

/**
 * CPT
 */

// partenaires

function wpm_custom_post_type()
{

    // partenaires
    $partenaires_labels = array(
        // Le nom au pluriel
        'name'                => _x('Partenaires', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Partenaire', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Partenaires'),
        // Les différents libellés de l'administration
        'all_items'           => __('Tous les partenaires'),
        'view_item'           => __('Voir les partenaires'),
        'add_new_item'        => __('Ajouter une nouvelle série TV'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer la partenaires'),
        'update_item'         => __('Modifier la partenaires'),
        'search_items'        => __('Rechercher un partenaire'),
        'not_found'           => __('Non trouvée'),
        'not_found_in_trash'  => __('Non trouvée dans la corbeille'),
    );

    $partenaires_args = array(
        'label'               => __('Partenaires'),
        'description'         => __('Tous sur partenaires'),
        'labels'              => $partenaires_labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        /* 
		* Différentes options supplémentaires
		*/
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'              => array('slug' => 'partenaires'),

    );

    register_post_type('partenaires', $partenaires_args);

    // Exhibits
    $exposants_labels = array(
        // Le nom au pluriel
        'name'                => _x('Exposants', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Exposants', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Exposants'),
        // Les différents libellés de l'administration
        'all_items'           => __('Tous les exposants'),
        'view_item'           => __('Voir les exposants'),
        'add_new_item'        => __('Ajouter un nouvel exposant'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer la exposants'),
        'update_item'         => __('Modifier la exposants'),
        'search_items'        => __('Rechercher un exposant'),
        'not_found'           => __('Non trouvée'),
        'not_found_in_trash'  => __('Non trouvée dans la corbeille'),
    );

    $exposants_args = array(
        'label'               => __('Exposants'),
        'description'         => __('Tous sur exposants'),
        'labels'              => $exposants_labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        /* 
		* Différentes options supplémentaires
		*/
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'              => array('slug' => 'exposants'),

    );

    register_post_type('exposants', $exposants_args);

    // Sponsors
    $sponsors_labels = array(
        // Le nom au pluriel
        'name'                => _x('Sponsors', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Sponsors', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Sponsors'),
        // Les différents libellés de l'administration
        'all_items'           => __('Tous les sponsors'),
        'view_item'           => __('Voir les sponsors'),
        'add_new_item'        => __('Ajouter une nouvelle sponsors'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer le sponsor'),
        'update_item'         => __('Modifier le sponsor'),
        'search_items'        => __('Rechercher un sponsor'),
        'not_found'           => __('Non trouvé'),
        'not_found_in_trash'  => __('Non trouvé dans la corbeille'),
    );

    $sponsors_args = array(
        'label'               => __('Sponsors'),
        'description'         => __('Tous sur sponsors'),
        'labels'              => $sponsors_labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        /* 
        * Différentes options supplémentaires
        */
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'              => array('slug' => 'sponsors'),

    );

    register_post_type('sponsors', $sponsors_args);

    // Infos pratiques
    $ip_labels = array(
        // Le nom au pluriel
        'name'                => _x('Infos pratiques', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Infos pratiques', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Infos pratiques'),
        // Les différents libellés de l'administration
        'all_items'           => __('Toutes les infos'),
        'view_item'           => __('Voir les infos'),
        'add_new_item'        => __('Ajouter une nouvelle infos'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer l\'info'),
        'update_item'         => __('Modifier l\'info'),
        'search_items'        => __('Rechercher une info'),
        'not_found'           => __('Non trouvée'),
        'not_found_in_trash'  => __('Non trouvée dans la corbeille'),
    );

    $ip_args = array(
        'label'               => __('Infos Pratiques'),
        'description'         => __('Tous sur infos'),
        'labels'              => $ip_labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        /* 
    * Différentes options supplémentaires
    */
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'              => array('slug' => 'infos'),

    );

    register_post_type('infos', $ip_args);


    // Cargos
    $cargos_labels = array(
        // Le nom au pluriel
        'name'                => _x('Cargos', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Cargos', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Cargos'),
        // Les différents libellés de l'administration
        'all_items'           => __('Tous les cargos'),
        'view_item'           => __('Voir les cargos'),
        'add_new_item'        => __('Ajouter une nouveau cargo'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer le cargo'),
        'update_item'         => __('Modifier le cargo'),
        'search_items'        => __('Rechercher un cargo'),
        'not_found'           => __('Non trouvé'),
        'not_found_in_trash'  => __('Non trouvé dans la corbeille'),
    );

    $cargos_args = array(
        'label'               => __('Cargos'),
        'description'         => __('Tous sur cargos'),
        'labels'              => $cargos_labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        /* 
        * Différentes options supplémentaires
        */
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'              => array('slug' => 'cargos'),

    );

    register_post_type('cargos', $cargos_args);

    // Galleries
    $galeries_labels = array(
        // Le nom au pluriel
        'name'                => _x('Galerie', 'Post Type General Name'),
        // Le nom au singulier
        'singular_name'       => _x('Galerie', 'Post Type Singular Name'),
        // Le libellé affiché dans le menu
        'menu_name'           => __('Galerie'),
        // Les différents libellés de l'administration
        'all_items'           => __('Toutes les galeries'),
        'view_item'           => __('Voir les galeries'),
        'add_new_item'        => __('Ajouter une nouvelle galerie'),
        'add_new'             => __('Ajouter'),
        'edit_item'           => __('Editer la galerie'),
        'update_item'         => __('Modifier la galerie'),
        'search_items'        => __('Rechercher une galerie'),
        'not_found'           => __('Non trouvée'),
        'not_found_in_trash'  => __('Non trouvée dans la corbeille'),
    );

    $galeries_args = array(
        'label'               => __('Galeries'),
        'description'         => __('Tous sur galeries'),
        'labels'              => $galeries_labels,
        // On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
        'supports'            => array('title', 'editor', 'thumbnail'),
        /* 
    * Différentes options supplémentaires
    */
        'show_in_rest' => true,
        'hierarchical'        => false,
        'public'              => true,
        'has_archive'         => true,
        'rewrite'              => array('slug' => 'galeries'),

    );

    register_post_type('galeries', $galeries_args);
}

add_action('init', 'wpm_custom_post_type', 0);
