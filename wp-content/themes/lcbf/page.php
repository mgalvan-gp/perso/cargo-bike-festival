    <?php get_header(); ?>
    <div class="row vh-max">
        <div class="col-xl-5 p-0 page-hero-img d-flex align-items-center">
            <div class="mx-auto">
                <div class="d-flex align-items-end">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Logo-CBF-Menu.png" alt="Image d'en-tête" class="img-fluid"></a>

                    <?php wp_nav_menu(array('menu' => 'rs')); ?>
                </div>
            </div>
        </div>
        <div class="col-xl-7">
            <div class="text-end p-2">
                <a href="<?php bloginfo('url'); ?>"><i class="fa-solid fa-xmark fa-2x"></i></a>
            </div>
            <div class="row align-items-center">
                <div class="col-12 col-md-8 mx-auto mb-5 forms">
                    <h3 class="d-flex align-items-center"><img class="revert" src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""><span class="mx-3"><?php the_title(); ?></span><img src="<?php echo get_template_directory_uri(); ?>/images/Picto-titre-bleu-02.png" alt="Card image cap" alt=""></h3>
                    <div class="contact-form">
                        <?php if (is_page('connexion')) : ?>
                            <?php echo do_shortcode('[wpmem_form login redirect_to=https://cargobikefestival.fr]'); ?>
                        <?php elseif (is_page('mon-compte')) : ?>
                            <?php echo do_shortcode('[wpmem_profile]'); ?>
                        <?php elseif (is_page('contact')) : ?>
                            <?php echo do_shortcode('[contact-form-7 id="310" title="Formulaire de contact 1"]'); ?>
                        <?php elseif (is_page('inscription')) : ?>
                            <?php echo do_shortcode('[wpmem_form register]'); ?>
                        <?php elseif (is_page('devenir-benevole')) : ?>
                            <?php
                            $comment_devenir_benevole = get_post(300);
                            $cdb_title = $comment_devenir_benevole->post_title;
                            $cdb_content = $comment_devenir_benevole->post_content;
                            ?>

                            <p>
                                <?php echo $cdb_content ?>
                            </p>

                            <?php echo do_shortcode('[contact-form-7 id="77" title="Devenir bénévole"]'); ?>
                        <?php elseif (is_page('devenir-partenaire')) : ?>
                            <?php
                            $comment_devenir_partenaire = get_post(302);
                            $cdp_title = $comment_devenir_partenaire->post_title;
                            $cdp_content = $comment_devenir_partenaire->post_content;
                            ?>

                            <p>
                                <?php echo $cdp_content ?>
                            </p>

                            <?php echo do_shortcode('[contact-form-7 id="78" title="Devenir partenaire"]'); ?>
                        <?php elseif (is_page('devenir-exposant')) : ?>
                            <?php
                            $comment_devenir_exposant = get_post(304);
                            $cde_title = $comment_devenir_exposant->post_title;
                            $cde_content = $comment_devenir_exposant->post_content;
                            ?>

                            <p>
                                <?php echo $cde_content ?>
                            </p>
                            <?php echo do_shortcode('[contact-form-7 id="79" title="Devenir exposant"]'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>