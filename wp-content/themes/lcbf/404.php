<?php get_header(); ?>
<section id="not-found" class="vh-max-no-footer">
    <p class="my-5">Oops, page introuvable</p>

    <div class="text-center">
        <img src="<?php echo get_template_directory_uri(); ?>/images/Illustration-404.png" alt="">
    </div>
</section>
<?php get_footer(); ?>