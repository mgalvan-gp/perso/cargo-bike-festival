<?php get_header(); ?>

<div class="py-5">
    <h3 class="text-center mb-5"><?php the_title(); ?></h3>
    <div id="postThumbnail" class="text-center"><?php echo get_the_post_thumbnail($post->ID); ?></div>
    <div class="w-75 mx-auto mt-5"><?php the_content(); ?></div>
</div>

<?php get_footer(); ?>