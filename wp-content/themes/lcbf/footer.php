<footer class="py-5">
    <div class="col-md-10 mx-auto">
        <div class="row m-0">
            <div class="col-lg-3 mb-3">
                <div class="mb-2">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/Logo-CBF-Menu.png" alt="Image d'en-tête" class="img-fluid">
                    </a>
                </div>
                <h5>Suivez-nous !</h5>
                <div id="footer-networks">
                    <?php wp_nav_menu(array('menu' => 'rs')); ?>
                </div>
            </div>
            <div class="col-lg-2">
                <h5>Le festival</h5>
                <?php wp_nav_menu(array('menu' => 'Footer1')); ?>
            </div>

            <div class="col-lg-2">
                <h5>Participer</h5>
                <?php wp_nav_menu(array('menu' => 'Footer2')); ?>
            </div>

            <div class="col-lg-2">
                <h5>Liens</h5>
                <?php wp_nav_menu(array('menu' => 'Footer3')); ?>
            </div>

            <div id="election-item" class="col-lg-3">
                <div class="row">
                    <div class="col-xl-3 p-0">
                        <img class="election" src="<?php echo get_template_directory_uri(); ?>/images/Picto-election-cargo-2023.png" alt="Card image cap">
                    </div>
                    <div class="col-xl-7 p-0 mt-2">
                        <h5>Élection du vélo cargo de l’année 2023</h5>
                        <a href="<?php echo bloginfo('url'); ?>/election/">Je vote !</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>